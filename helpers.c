#include <stdint.h>
#include <msp430.h>
#include "helpers.h"
#include "half-sine.h"
#include "vars.h"

/*
 * helpers.c
 *
 *  Created on: 28.09.2016.
 *      Author: Roberts Gotlaufs
 *
 *  Various helper functions.
 *  Used in animation calculations.
 */

uint8_t getHalfSine(const int16_t angle){
    /*
     * Returns value from optimized half-sine.c containing 240 values
     * Accepts angle up to 360
     *
     */
    uint8_t value;

    if (angle > 240 - 1)
        value = 0;
    else
        value = h_sine[angle];

    return value;
}

uint16_t generate_random(const uint16_t max){
    /*
     * Generate pseudo-random value between 0 and 'max'.
     *
     * Uses ADC10
     *
     * Return is a unsigned int. Resolution is 8 bits.
     */
    static const int8_t bits = 8;
    uint8_t seed, index = bits;
    static uint32_t number;

    for (; index != 0; --index){
        ADC_start();
        seed += (ADC10MEM & 1) << (index - 1);
    }

    // Scale to max
    number = ((uint32_t)(seed) * max) /255;
    return (uint16_t)number;
}

void init_ADC(void){
    /*
     * Initialize the ADC for random number generation
     */

    // Read more at:
    // http://xdec.de/msp430-random-number-generator-rng/

    ADC10CTL0 &= ~ENC;    // conversion disabled
    //Channel A1; ADC10 CLK divider = /8; CLK=ADC10OSC
    ADC10CTL1 = 0x0001 + ADC10DIV_7 + ADC10SSEL_0;
    //Vref+=Vcc (3.3V)&Vss=Vr- ; S&H time 16x; ADC10 on
    ADC10CTL0 = SREF_0 + ADC10SHT_3 + ADC10ON;
    // P1.1 is analog input (floating port)
    ADC10AE0 |= BIT1;
}

void ADC_start(void){
    /*
     * Start the ADC
     */
    ADC10CTL0 |= ENC + ADC10SC;         // start & enable conversion
    while(ADC10CTL0 & ADC10BUSY);       // ADC busy?
}

void fill_with_random(const uint8_t max){
    /*
     * Fill both future and current animation memories with random data
     */
    uint8_t i = NUM_LEDS;
    for(; i != 0; --i){
        anim_mem_future[i] = (uint8_t)(generate_random(max));
        anim_mem_curr[i] = (uint8_t)(generate_random(max));
    }
}

uint8_t memory_step(const uint8_t i, const uint8_t random_max){
    /*
     * Step the anim_mem_curr[i] one step to the anim_mem_future[i].
     *
     * Also initialized memory if needed.
     * Returns the new anim_mem_curr[i]
     */
    // First generate data if not initialized
    if (memory_initialized == 0){
        fill_with_random(random_max);
        memory_initialized = 1;
    }

    if (anim_mem_curr[i] == anim_mem_future[i])
        anim_mem_future[i] = (uint8_t)(generate_random(random_max));
    else if (anim_mem_curr[i] > anim_mem_future[i])
        --anim_mem_curr[i];
    else
        ++anim_mem_curr[i];

    return anim_mem_curr[i];
}
