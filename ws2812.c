#include <stdint.h>
#include <msp430.h>
#include "ws2812.h"
#include "vars.h"
#include "helpers.h"

/*
 * ws2812.c
 *
 *  Created on: 21.09.2016.
 *      Author: Michael Meli
 *              Roberts Gotlaufs
 *  Original source:
 *      https://github.com/mjmeli/MSP430-NeoPixel-WS2812-Library
 *
 *  ws2812 driver library using MSP430 SPI module.
 */

typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} LED;

#define OUTPUT_PIN  (0x80)          // Set to whatever UCB0SIMO is on your processor (Px.7 here)


LED leds[NUM_LEDS] = {{0,0,0}};

// Initializes everything needed to use this library. This clears the strip.
void initStrip(){
    P1SEL |= OUTPUT_PIN;            // configure output pin as SPI output
    P1SEL2 |= OUTPUT_PIN;
    UCB0CTL0 |= UCCKPH + UCMSB + UCMST + UCSYNC;    // 3-pin, MSB, 8-bit SPI master
    UCB0CTL1 |= UCSSEL_2;           // SMCLK source (16 MHz)
    UCB0BR0 = 3;                    // 16 MHz / 3 = .1875 us per bit
    UCB0BR1 = 0;
    UCB0CTL1 &= ~UCSWRST;           // Initialize USCI state machine

    clearStrip();                   // clear the strip
}

// Sets the color of a certain LED (0 indexed)
void setLEDColor(uint16_t p, uint8_t r, uint8_t g, uint8_t b){
//  leds[p].red = r;
//  leds[p].green = g;
//  leds[p].blue = b;
// Chinese LED's have different sequence
    leds[p].red = g;
    leds[p].green = r;
    leds[p].blue = b;
}

// Set the color of a certain LED by HSV Hue angle (0 indexed)
void setHsvColor(const uint8_t number, int16_t angle){
    /*
     * Wrapper for setLEDColor to use HSV angle value.
     *
     * Accepts angle in full signed int range and will
     * auto wrap to [0..360] range before calculations
     */

    // Check negative angle
    while (angle < 0)
        angle += 360;
    // Check over 360 angle
    while (angle >= 360)
        angle -= 360;

    setLEDColor(number, getHalfSine((angle+120)%360),
                getHalfSine(angle),
                getHalfSine((angle+240)%360));
}

// Send colors to the strip and show them. Disables interrupts while processing.
void showStrip(){
    __bic_SR_register(GIE);         // disable interrupts
    
    // send RGB color for every LED
    uint16_t i;
    uint8_t j;

    for (i = 0; i < NUM_LEDS; ++i){
        uint8_t rgb[3] = {leds[i].green, leds[i].red, leds[i].blue}; // get RGB color for this LED

        // send green, then red, then blue
        for (j = 0; j < 3; ++j){
            uint8_t mask = 0x80;                 // b1000000

            // check each of the 8 bits
            while(mask != 0){
                while (!(IFG2 & UCB0TXIFG));    // wait to transmit

                if (rgb[j] & mask){             // most significant bit first
                    UCB0TXBUF = HIGH_CODE;      // send 1
                }
                else {
                    UCB0TXBUF = LOW_CODE;       // send 0
                }

                mask >>= 1;                     // check next bit
            }
        }
    }

    // send RES code for at least 50 us (800 cycles at 16 MHz)
    _delay_cycles(800);

    __bis_SR_register(GIE);         // enable interrupts
}

// Push all the colors of LED's forward (to next LED) and load the new color in the first LED
void advanceStrip(uint8_t r, uint8_t g, uint8_t b){
    int16_t i;

    for (i=NUM_LEDS-2; i>=0; i--){
        // Copy all data one step forward
        leds[i + 1].red = leds[i].red;
        leds[i + 1].green = leds[i].green;
        leds[i + 1].blue = leds[i].blue;
    }
    // Load new data in first pos
    leds[0].red = r;
    leds[0].green = g;
    leds[0].blue = b;

    showStrip();  // Refresh strip
}

// Clear the color of all LEDs (make them black/off)
void clearStrip(){
    fillStrip(0x00, 0x00, 0x00);    // black (all off)
}

// Fill the strip with a solid color. This will update the strip.
void fillStrip(uint8_t r, uint8_t g, uint8_t b){
    uint16_t i;
    for (i = 0; i < NUM_LEDS; ++i){
        setLEDColor(i, r, g, b);        // set all LEDs to specified color
    }
    showStrip();                        // refresh strip
}
