#include <stdint.h>
/*
 * helpers.h
 *
 *  Created on: 28.09.2016.
 *      Author: Roberts Gotlaufs
 *
 *  Various helper functions.
 *  Used in animation calculations.
 */

#ifndef HELPERS_H_
#define HELPERS_H_

// Get value of half-rectified sine from lookup table
uint8_t getHalfSine(const int16_t angle);

// Generate a rendom number up to 'max' using ADC LSB noise
uint16_t generate_random(const uint16_t max);

// Fill animation memories (anim_mem_future and anim_mem_curr)
// with random data up to 'max'
void fill_with_random(const uint8_t max);

// Set up ADC for random number generation
void init_ADC(void);

// Take ADC sample. Sample must be read from 'ADC10MEM' register.
void ADC_start(void);

// Advance the 'i' element of anim_mem_curr towards the 'i' element
// of anim_mem_future. When reached, will generate new 'i' number
// up to max.
// Returns the incremented 'i' value from anim_mem_curr.
uint8_t memory_step(const uint8_t i, const uint8_t random_max);

#endif /* HELPERS_H_ */
