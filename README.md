# LED virtene #
This is a simple WS2812B clone (APA APA106 F5) strip project with various lighting effects for MSP430.

### How do I get set up? ###

This project was created using Texas Instruments Code Composer Studio v6.
That should be all you need.

Find the datasheed for the LED modules used in the root directory.

### Board files ###

There is a simple PCB  project using through-hole parts created in DipTrace.
Find it in folder 'PCB'.
