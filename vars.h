#include <stdint.h>
/*
 * vars.h
 *
 *  Created on: 2016. gada 28. aug.
 *      Author: Robers Gotlaufs
 *
 *  All the global variables and switches are declared/defined here.
 */

#ifndef VARS_H_
#define VARS_H_
/*
 * Debugging switch.
 *
 * If this is defined, expect the following:
 *      * P1.6 will be set up as output and button press will toggle it
 */
//#define debugging yes

// Global constants
#define NUM_LEDS 78         // Number of LED's in the strip

#define LED_PIN BIT0        // Status LED pin on Port 1
#define BUTTON_PIN BIT3     // Button pin on Port 1
#define LOOP_DELAY 50000    // This is delay in cycles for each animation frame call

#define DEFAULT_ANIM RAINBOW    // Default animation on power-on

#ifdef debugging
#define DEBUG_LED BIT6
#endif


enum animation {
    RAINBOW      = 0,   // Slow rainbow animation moving along the strip
    YELLOW_RED   = 1,   // Random smooth changing yellow-red tones
    WHITE        = 2,   // Random smooth changing intensities of white
    GRADUAL_FILL = 3,   // Filling of the strip LED-by-LED with predefined colors
    STANDARD     = 4,   // Standard cheap chrstmas lights animation
    RUNNING      = 5,   // Groups of (n) random color LED's moving along the strip
    PULSING      = 6,   // Whole strip same color, pulse in brightness, change color when dimmer
    RAINBOW_FAST = 7,   // Shorter (length) and faster (speed) running rainbow
    GREEN_TINGLE = 8,   // Shades of green randomly without smooth transitions
    ALL_COLORS   = 9,   // All line same color full brightness, full Hue circle (rainbow)
    YELLOW       = 10   // Random smooth changing yellow tones. Practically the same as YELLOW-RED.
};

extern volatile int8_t reset;                     // Internal reset flag
extern int8_t memory_initialized;
extern volatile enum animation animation_state;

extern uint8_t anim_mem_future[NUM_LEDS]; // Byte memories for more complex animations
extern uint8_t anim_mem_curr[NUM_LEDS];

#endif /* VARS_H_ */
