#include <msp430.h> 
#include "ws2812.h"
#include "animations.h"
#include "helpers.h"
#include "vars.h"

/*
 * main.c
 *
 *  Created on: 24.07.2016.
 *      Author: Roberts Gotlaufs
 *
 *  Christmas lights using addressable WS2812B equivalaent (APA106 F5) RGB diode modules.
 *
 *  Lights are a simple chained strip. Animation states are controlled by a button.
 *
 */

/*
 * FUTURE:
 *
 *      * Change loops to count down
 */

int main(void) {
    WDTCTL = WDTPW + WDTHOLD;               // Stop WDT
    if (CALBC1_16MHZ==0xFF)                 // If calibration constant erased
    {
        while(1);                           // do not load, trap CPU!!

    }

    // Configure clock to 16 MHz
    BCSCTL1 = CALBC1_16MHZ;
    DCOCTL = CALDCO_16MHZ;

    // Turn on Green status LED
    P1DIR |= LED_PIN;
    P1OUT |= LED_PIN;

    // Initialize Button
    P1DIR &= ~BUTTON_PIN;   // Button is input
    P1IES |= BUTTON_PIN;    // Interrupt on HIGH -> LOW
    P1IE |= BUTTON_PIN;     // Enable Interrupt for the button
    __enable_interrupt();   // Enable Interrupt handling

    // Initialize the ADC for random number generation
    init_ADC();

    #ifdef debugging
    P1DIR |= DEBUG_LED;
    P1OUT |= DEBUG_LED;
    #endif

    // initialize LED strip
    initStrip();

    // MainLoop
    while(1)
    {
        // Animation state machine.
        //
        // States changed by ISR.
        // Each call to animation function will advance the respective
        // animation by one frame.

        switch (animation_state){
            case RAINBOW : {
                rainbow(0);
                break;
            }
            case YELLOW_RED :{
                yellow(1);
                break;
            }
            case WHITE :{
                white();
                break;
            }
            case GRADUAL_FILL : {
                gradualFill();
                break;
            }
            case STANDARD : {
                standard();
                break;
            }
            case RUNNING : {
                running();
                break;
            }
            case PULSING : {
                pulsing();
                break;
            }
            case RAINBOW_FAST : {
                rainbow(1);
                break;
            }
            case GREEN_TINGLE : {
                greenTingle();
                break;
            }
            case ALL_COLORS : {
                allColors();
                break;
            }
            case YELLOW : {
                yellow(0);
                break;
            }
            default :
                animation_state = DEFAULT_ANIM;
                break;
        }

        // Handle animation reset
        if (reset == 1){
            // Uncomment this line to clear the strip on each animation
            // change.
            //clearStrip();
            memory_initialized = 0;
            reset = 0;
        }

        // Lazy animation frame delay
        // Should change this to timer/wakeup
        _delay_cycles(LOOP_DELAY);
    }
}

/*
 * ISR
 */

#pragma vector = PORT1_VECTOR
__interrupt void button(void){
    /*
     * Button ISR
     *
     * Each button press will change the animation (state) to the next one
     * and resets on overflow and set the 'reset' flag to reset the animation.
     * ISR also features crude switch debaunce SW elay.
     */

    #ifdef debugging
    P1OUT ^= DEBUG_LED;
    #endif

    reset = 1;
    ++animation_state;
    if (animation_state > 10)
        // Reset animation to the first one on overflow
        animation_state = 0;

    // Crude switch debounce
    __delay_cycles(10000000);

    // Clear interrupt register for this button
    P1IFG &= ~BUTTON_PIN;
}
