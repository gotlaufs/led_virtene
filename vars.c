#include <stdint.h>
#include "vars.h"

/*
 * vars.c
 *
 *  Created on: 2016. gada 28. aug.
 *      Author: Roberts Gotlaufs
 */

// Animation reset flag.
// When this is '1', the animation will reset on start (and put this to '0')
// This flag gets set only by animation changer (button)
volatile int8_t reset = 0;

// This gets set when memory is initialized with valid data
int8_t memory_initialized = 0;

// Current animation state
volatile enum animation animation_state = DEFAULT_ANIM;

// Simple byte memory for each LED for more complex animations.
// Two separate ones for tracking animation progress.
uint8_t anim_mem_future[NUM_LEDS];
uint8_t anim_mem_curr[NUM_LEDS];
