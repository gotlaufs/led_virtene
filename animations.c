#include <stdint.h>
#include "ws2812.h"
#include "animations.h"
#include "vars.h"
#include "helpers.h"

/*
 * animations.c
 *
 *  Created on: 28.08.2016.
 *      Author: Roberts Gotlaufs
 *
 *  Animation function defines.
 *  Each call to animation function will advance it by one frame.
 *
 *  Use 'ADDITIONAL_DELAY' in animation function code to
 *  increase/decrease speed of the animations.
 *
 *  Each animation operates directly with 'ws2812' library calls.
 */

void rainbow(const int8_t fast){
    /*
     * Rainbow effect on LED's
     *
     * Uses lookup table from half-sine.c
     * Rainbow is done using HSV color coding
     *
     * If fast = 1, rainbow will be much shorter (repeat period on strip)
     * and move faster (No delays)
     *
     * setHsvColor() will wrap to 360 degrees automatically.
     */
    uint8_t ADDITIONAL_DELAY;

    static int16_t angle;
    int16_t i, local_angle;
    static int16_t rainbow_offset;
    static int8_t delay_counter;  // Additional delay counter

    // Switch for fst/slow operation
    if (fast == 0){
        rainbow_offset = 9;     // Rainbow repeat period on LED strip
        ADDITIONAL_DELAY = 10;
    }
    else if (fast == 1){
        rainbow_offset = 36;    // Rainbow repeat period on LED strip
        ADDITIONAL_DELAY = 0;
    }

    if (delay_counter > ADDITIONAL_DELAY){
        delay_counter = 0;

        for (i=0; i<NUM_LEDS; ++i){
            local_angle = angle + i * rainbow_offset;   // This particular LED's offset

            // Set Colors
            setHsvColor(i, local_angle);
        }
        showStrip();

        ++angle;
        if (angle >= 360)
                angle = 0;
        }
    else
        ++delay_counter;
}

void yellow(const uint8_t red){
    /*
     * Animation using yellow colors.
     *
     * Each LED is assigned a random Hue value from defined range and
     * another random starting value. The starting value is then
     * incremented by single step until it reaches first value (future).
     * Then a new future value is generated.
     *
     * Has argument 'red'.
     * If '1', will display yellow to red tones
     * If '0', will display less red tones.
     */
    static const uint8_t ADDITIONAL_DELAY = 15;
    // Color angle span (Hue) of the HSV model
    static uint8_t ANGLE_SPAN;
    // Color offset - all colors will be moved up (positive) or
    // down (negative) by this amount.
    static int8_t ANGLE_OFFSET;

    // Assigning values based on the parameter
    if (red == 1){
        // More 'red' values.
        ANGLE_SPAN = 55;
        ANGLE_OFFSET = 10;
    }
    else{
        // More 'yellow' values.
        ANGLE_SPAN = 35;
        ANGLE_OFFSET = 30;
    }

    int16_t angle;
    uint8_t i;
    static int8_t delay_counter; // Additional delay counter

    if (delay_counter > ADDITIONAL_DELAY){
        delay_counter = 0;
        // Advance the animation
        for (i = 0; i < NUM_LEDS; ++i){
            // Add offset and wrap to 360
            angle = memory_step(i, ANGLE_SPAN) + ANGLE_OFFSET;
            if (angle < 0)
                angle += 360;

            // Set appropriate color
            setHsvColor(i, angle);
        }
        showStrip();
    }
    else
        ++delay_counter;
}

void white(void){
    /*
     * Animation using white colors
     *
     * Operating principle is similar to yellow(), but instead of color,
     * the brightnes (intensity) of each LED randomly varies
     */

    // Brightness variation is expressed from 0 to 100 where 0 means
    // no variation at all (always full brightness) and 100 means
    // variation from Full brightness to LED off
    static const uint8_t BRIGHTNESS_VARIATION = 95;
    static const uint8_t ADDITIONAL_DELAY = 10;

    uint8_t i, brightness;
    static int8_t delay_counter; // Additional delay counter

    if (delay_counter > ADDITIONAL_DELAY){
        delay_counter = 0;

        // Advance the animation
        for (i = 0; i < NUM_LEDS; ++i){
            // Add offset and wrap to 360
            brightness = 100 - memory_step(i, BRIGHTNESS_VARIATION);

            // Set led to white but adjust the brightness
            setLEDColor(i, (int16_t)(255)*brightness/100,
                        (int16_t)(255)*brightness/100,
                        (int16_t)(255)*brightness/100);
        }
        showStrip();
    }
    else
        ++delay_counter;
}

void gradualFill(void){
    /*
     * Gradual filling of the led strip with defined colors.
     *
     */
    // This must be changed acording to color count
    // colors[rgb][number_of_different_colors]
    // colors[3][2]
    // colors[3][15]
    #define num_colors  (3)

    // Colors to be displayed in RGB format. Each color is a new line.
    static const int8_t colors[3][num_colors] = {
            {0x00, 0xFF, 0x00},  // Green
            {0x00, 0x00, 0xFF},  // Blue
            {0xFF, 0x00, 0x00}   // Red
    };

    const uint8_t ADDITIONAL_DELAY = 100;

    static int8_t current_color = 0; // First color
    static int8_t current_led   = 0; // First LED
    static int8_t delay_counter = 0; // Additional delay counter

    if (delay_counter > ADDITIONAL_DELAY){
        delay_counter = 0;

        setLEDColor(current_led, colors[0][current_color],
                    colors[1][current_color],
                    colors[2][current_color]);
        showStrip();

        // Increment counters
        ++current_led;
        if (current_led >= NUM_LEDS){
            current_led = 0;
            ++current_color;
        }
        if (current_color >= num_colors){
            current_color = 0;
        }
    }

    else
        ++delay_counter;
}

void standard(void){
    /*
     * Standard cheap Chinese christmas light animation
     *
     * Whole strip is divided in segments of three LED's
     * Have three colors, 2 are visible and they slowly alternate
     * without smooth transition:
     * [C1 C2 --]
     * [-- C2 C3]
     * [C1 -- C3]
     * Repeat
     */

    // The three colors in RGB format
    static const int8_t colors[3][3] = {
            {0xFF, 0x00, 0x00},     // Red
            {0x00, 0xFF, 0x00},     // Green
            {0x00, 0x00, 0xFF}      // Blue
    };

    static const uint8_t ADDITIONAL_DELAY = 200;

    static uint8_t delay_counter, current_off;
    static uint8_t i, three_counter;

    if (delay_counter > ADDITIONAL_DELAY){
        // Reset the delay
        delay_counter = 0;

        for (i = 0; i < NUM_LEDS; ++i){
            if (current_off == three_counter)
                // Off
                setLEDColor(i, 0x00, 0x00, 0x00);
            else
                setLEDColor(i, colors[0][three_counter],
                            colors[1][three_counter],
                            colors[2][three_counter]);

            ++three_counter;
            if (three_counter > 3-1)
                three_counter = 0;
        }

        ++current_off;
        if (current_off > 3-1)
            current_off = 0;

        showStrip();

        #ifdef debugging
        P1OUT ^= LED_PIN;
        #endif
    }
    else
        ++delay_counter;
}

void running(void){
    /*
     * Segments of 'SEGMENT_WIDTH' LED's run across the whole strip
     * in random color (Hue).
     *
     */
    // Number of LED's in one running segment
    static const int8_t SEGMENT_WIDTH = 3;
    static const uint8_t ADDITIONAL_DELAY = 70;

    static int16_t angle;           // Remember last color
    static int8_t curr_led = 0;   // Current led of segment

    static int8_t delay_counter;

    if (delay_counter > ADDITIONAL_DELAY){
        // Reset the delay
        delay_counter = 0;

        if (curr_led != 0)
            --curr_led;
        else{
            // '-1' for segments to be 'SEGMENT_WIDTH', not +1 long
            curr_led = SEGMENT_WIDTH - 1;
            angle = generate_random(360);
        }

        // Push color onto the strip
        advanceStrip(getHalfSine((angle+120)%360),
                    getHalfSine(angle),
                    getHalfSine((angle+240)%360));

    }
    else
        ++delay_counter;
}

void pulsing(void){
    /*
     * Whole strip pulsing Color is the same for all the LED's
     * but it changes when stip is at the darkest.
     */
    // Animation cycle count to reach full brightness
    static const uint8_t PULSE_HALF_PERIOD = 255;
    static const uint8_t ADDITIONAL_DELAY = 5;

    static uint8_t i, up_down;
    static int16_t angle;
    // up_down = 0 = DOWN
    static int8_t delay_counter;

    if (delay_counter > ADDITIONAL_DELAY){
        // Reset the delay
        delay_counter = 0;

        if (i == 0){
            // Reached lowest brightness, change color and count up
            angle = generate_random(360);   // New color
            up_down = 1;                    // Up
        }
        if (i == PULSE_HALF_PERIOD)
            // Reached full brightness, count down
            up_down = 0;

        // Calculate brightness and display the strip
        //
        // Brightness = Icurrent * ColorBrightness / Imax
        // Imax = PULSE_HALF_PERIOD
        fillStrip(i * getHalfSine((angle+120)%360) / PULSE_HALF_PERIOD,
                  i * getHalfSine(angle) / PULSE_HALF_PERIOD,
                  i * getHalfSine((angle+240)%360) / PULSE_HALF_PERIOD);

        if (up_down == 0)
            --i;
        else if (up_down == 1)
            ++i;
    }
    else
        ++delay_counter;
}

void greenTingle(void){
    /*
     * Green tingling animation.
     *
     * Each led is randomly updated with shades of green without transitions.
     *
     */
    // Color angle (Hue) span for the HSV color model.
    static const uint8_t ANGLE_SPAN = 95;
    // Color offset - all colors will be moved up (positive) or
    // down (negative) by this amount.
    static const int8_t ANGLE_OFFSET = 45;
    static const uint8_t ADDITIONAL_DELAY = 50;

    int16_t angle;
    uint8_t i;
    static int8_t delay_counter;

    // Generate random greens
    // And fill strip with it
    if (memory_initialized == 0){
        fill_with_random(ANGLE_SPAN);
        memory_initialized = 1;

        for (i = 0; i < NUM_LEDS; ++i)
            setHsvColor(i, anim_mem_curr[i] + ANGLE_OFFSET);
    }

    if (delay_counter > ADDITIONAL_DELAY){
        // Reset the delay
        delay_counter = 0;

        // Pick random LED and set random shade to it
        i = generate_random(NUM_LEDS);
        angle = generate_random(ANGLE_SPAN);

        setHsvColor(i, angle + ANGLE_OFFSET);

        showStrip();
    }
    else
        ++delay_counter;
}

void allColors(void){
    /*
     * Simple rainbow animation where all strip is the same color
     * but the color is going through the full Hue ring.
     */
    static const uint8_t ADDITIONAL_DELAY = 8;

    static int16_t angle = 0;
    static int8_t delay_counter;

    if (delay_counter > ADDITIONAL_DELAY){
        // Reset the delay
        delay_counter = 0;

        fillStrip(getHalfSine((angle+120)%360),
                  getHalfSine(angle),
                  getHalfSine((angle+240)%360));

        // Increase counter
        ++angle;
        if (angle > 360 - 1)
            angle = 0;
    }
    else
        ++delay_counter;
}
