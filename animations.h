#include <stdint.h>
/*
 * animations.h
 *
 *  Created on: 28.08.2016.
 *      Author: Roberts Gotlaufs
 *
 *  Animation function defines.
 *  Each call to animation function will advance it by one frame.
 *
 *  Use 'ADDITIONAL_DELAY' in animation function code to
 *  increase/decrease speed of the animations.
 *
 *  Each animation operates directly with 'ws2812' library calls.
 */

#ifndef ANIMATIONS_H_
#define ANIMATIONS_H_

void rainbow(const int8_t fast);
void yellow(const uint8_t red);
void white(void);
void gradualFill(void);
void standard(void);
void running(void);
void pulsing(void);
void greenTingle(void);
void allColors(void);

#endif /* ANIMATIONS_H_ */
